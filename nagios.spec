Name:           nagios
Version:        4.5.3
Release:        2
Summary:        Host/service/network monitoring program
License:        GPLv2
URL:            https://www.nagios.org/projects/nagios-core/
Source0:        https://github.com/NagiosEnterprises/nagioscore/archive/nagios-%{version}.tar.gz
Source1:        nagios.logrotate
Source2:        nagios.htaccess
Source3:        nagios.internet.cfg
Source4:        nagios.htpasswd
Source5:        nagios.upgrade_to_v4.ReadMe
Source6:        nagios.upgrade_to_v4.sh
Source8:        nagios.tmpfiles.conf
Source10:       printer.png
Source11:       router.png
Source12:       switch.png
Source13:       nagios.README.SELinux.rst
Source14:       nagios.te
Source15:       nagios.fc
Source16:       nagios.if
Patch0001:      nagios-0001-default-init.patch
Patch0002:      nagios-0002-Fix-installation-of-httpd-conf.d-config-file.patch
Patch0003:      nagios-0003-Install-config-files-too.patch
Patch0004:      nagios-0004-Fix-path-to-CGI-executables.patch
Patch0005:      nagios-0005-Fixed-path-to-passwd-file-in-Apache-s-config-file.patch
Patch0006:      nagios-0006-Added-several-images-to-the-sample-config-revb.patch
Patch0007:      nagios-0008-Add-cfg_dir-etc-nagios-conf.d-to-the-main-nagios-con.patch
Patch0008:      nagios-0009-fix-localstatedir-for-linux.patch
Patch0009:      nagios-0011-remove-rpmbuild.patch
Patch0010:      nagios-0012-fix-spool.patch
Patch0011:      nagios-0013-fix-plugin.patch
Patch0012:      nagios-0014-fix-uidgid.patch
BuildRequires:  doxygen gcc gperf libjpeg-devel libpng-devel zlib-devel gd-devel > 1.8
BuildRequires:  perl-generators perl(CPAN) perl(ExtUtils::MakeMaker) perl(ExtUtils::Embed)
BuildRequires:  perl(Test::Harness) perl(Test::More) perl(Test::Simple)
BuildRequires:  checkpolicy, selinux-policy-devel libtool systemd
BuildRequires:  openssl-devel ima-evm-utils

Requires:       httpd php mailx nagios-common
Requires:       nagios-plugins-ping nagios-plugins-load

%description
Nagios is a program that will monitor hosts and services on your
network.  It has the ability to send email or page alerts when a
problem arises and when a problem is resolved.  Nagios is written
in C and is designed to run under Linux (and some other *NIX
variants) as a background process, intermittently running checks
on various services that you specify.
The actual service checks are performed by separate "plugin" programs
which return the status of the checks to Nagios. The plugins are
available at https://github.com/nagios-plugins/nagios-plugins
This package provides the core program, web interface, and documentation
files for Nagios. Development files are built as a separate package.

%package common
Summary:        Provides common directories, uid and gid among nagios-related packages
Requires(pre):  shadow-utils
Requires(post): shadow-utils
Provides:       user(nagios)
Provides:       group(nagios)

%description common
Provides common directories, uid and gid among nagios-related packages.

%package devel
Summary:        Provides include files that Nagios-related applications may compile against
Requires:       nagios = %{version}-%{release}

%description devel
Nagios is a program that will monitor hosts and services on your
network. It has the ability to email or page you when a problem arises
and when a problem is resolved. Nagios is written in C and is
designed to run under Linux (and some other *NIX variants) as a
background process, intermittently running checks on various services
that you specify.This package provides include files that Nagios-related applications
may compile against.

%package selinux
Summary:          SELinux context for nagios
Requires:         nagios = %{version}-%{release}
Requires(post):   policycoreutils
Requires(postun): policycoreutils


%description selinux
SElinux context for nagios.

%package contrib
Summary:          Eventhandlers contributed to nagios
Requires:         nagios = %{version}-%{release}

%description contrib
Various contributed items used by plugins and other tools.

%prep
%autosetup -n nagioscore-nagios-%{version} -p1

install -p -m 0644 %{SOURCE10} %{SOURCE11} %{SOURCE12} html/images/logos/

%build
%configure \
    --prefix=%{_datadir}/%{name} \
    --exec-prefix=%{_localstatedir}/lib/%{name} \
    --libdir=%{_libdir}/%{name} \
    --bindir=%{_sbindir} \
    --datadir=%{_datadir}/%{name}/html \
    --libexecdir=%{_libdir}/%{name}/plugins \
    --localstatedir=%{_localstatedir} \
    --with-checkresult-dir=%{_localstatedir}/spool/%{name}/checkresults \
    --with-cgibindir=%{_libdir}/nagios/cgi \
    --sysconfdir=%{_sysconfdir}/%{name} \
    --with-cgiurl=/%{name}/cgi-bin \
    --with-command-user=apache \
    --with-command-group=apache \
    --with-gd-lib=%{_libdir} \
    --with-gd-inc=%{_includedir} \
    --with-htmlurl=/%{name} \
    --with-lockfile=%{_localstatedir}/run/%{name}/%{name}.pid \
    --with-mail=/usr/bin/mail \
    --with-initdir=%{_unitdir} \
    --with-init-type=systemd \
    --with-nagios-user=nagios \
    --with-nagios-grp=nagios \
    --with-template-objects \
    --with-template-extinfo \
    --enable-event-broker \
    STRIP=/bin/true
make %{?_smp_mflags} all
%{__make} dox
%{__make} %{?_smp_mflags} -C contrib

sed -e "s|/usr/lib/|%{_libdir}/|" %{SOURCE2} > %{name}.htaccess
cp -f %{SOURCE3} internet.cfg
cp -f %{SOURCE5} UpgradeToVersion4.ReadMe
cp -f %{SOURCE6} UpgradeToVersion4.sh
echo >> html/stylesheets/common.css

mkdir selinux
cp -p %{SOURCE14} selinux/%{name}.te
cp -p %{SOURCE15} selinux/%{name}.fc
cp -p %{SOURCE16} selinux/%{name}.if
%make_build -f %{_datadir}/selinux/devel/Makefile %{name}.pp
bzip2 -9 %{name}.pp

%install
make DESTDIR=%{buildroot} INIT_OPTS="" INSTALL_OPTS="" COMMAND_OPTS="" CGIDIR="%{_libdir}/%{name}/cgi-bin" CFGDIR="%{_sysconfdir}/%{name}" fullinstall
install -d -m 0755 %{buildroot}%{_bindir}
mv %{buildroot}%{_sbindir}/nagiostats %{buildroot}%{_bindir}/nagiostats
install -d -m 0755 %{buildroot}%{_sysconfdir}/%{name}/private
mv %{buildroot}%{_sysconfdir}/%{name}/resource.cfg %{buildroot}%{_sysconfdir}/%{name}/private/resource.cfg
install -D -m 0644 %{SOURCE4} %{buildroot}%{_sysconfdir}/%{name}/passwd
install -D -m 0644 include/locations.h %{buildroot}%{_includedir}/%{name}/locations.h
install -D -m 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/logrotate.d/%{name}
install -d -m 0755 %{buildroot}%{_libdir}/%{name}/plugins/eventhandlers
install -d -m 0755 %{buildroot}%{_libdir}/%{name}/plugins/eventhandlers/distributed-monitoring/
install -d -m 0755 %{buildroot}%{_libdir}/%{name}/plugins/eventhandlers/redundancy-scenario1/
install -d -m 0775 %{buildroot}%{_localstatedir}/spool/%{name}/cmd
install -d -m 0775 %{buildroot}%{_localstatedir}/run/%{name}
install -d -m 0775 %{buildroot}%{_localstatedir}/run/%{name}
install -d -m 0775 %{buildroot}/%{_localstatedir}/log/
install -d -m 0775 %{buildroot}/%{_localstatedir}/log/%{name}/
install -d -m 0775 %{buildroot}/%{_localstatedir}/log/%{name}/archives
install -D -m 0644 -p %{SOURCE8} %{buildroot}%{_tmpfilesdir}/%{name}.conf
rm -f %{buildroot}%{_initrddir}/nagios

chmod 755 %{buildroot}%{_sbindir}/nagios
install -d -m 0755 %{buildroot}%{_datadir}/nagios/html/docs
%{__cp} -a Documentation/html/* %{buildroot}%{_datadir}/nagios/html/docs

install -pm 644 %{SOURCE13} README.SELinux.rst
install -D -m 0644 %{name}.pp.bz2 %{buildroot}%{_datadir}/selinux/packages/%{name}/%{name}.pp.bz2
install -D -p -m 0644 selinux/%{name}.if %{buildroot}%{_datadir}/selinux/devel/include/distributed/%{name}.if

make install -C contrib DESTDIR="%{buildroot}" INSTALL_OPTS=""
install -p -m 644 contrib/eventhandlers/disable_active_service_checks %{buildroot}%{_libdir}/nagios/plugins/eventhandlers/
install -p -m 644 contrib/eventhandlers/disable_notifications %{buildroot}%{_libdir}/nagios/plugins/eventhandlers/
install -p -m 644 contrib/eventhandlers/enable_active_service_checks %{buildroot}%{_libdir}/nagios/plugins/eventhandlers/
install -p -m 644 contrib/eventhandlers/enable_notifications %{buildroot}%{_libdir}/nagios/plugins/eventhandlers/
install -p -m 644 contrib/eventhandlers/submit_check_result %{buildroot}%{_libdir}/nagios/plugins/eventhandlers/

install -p -m 644 contrib/eventhandlers/distributed-monitoring/obsessive_svc_handler %{buildroot}%{_libdir}/nagios/plugins/eventhandlers/distributed-monitoring/
install -p -m 644 contrib/eventhandlers/distributed-monitoring/submit_check_result_via_nsca %{buildroot}%{_libdir}/nagios/plugins/eventhandlers/distributed-monitoring/
install -p -m 644 contrib/eventhandlers/redundancy-scenario1/handle-master-host-event %{buildroot}%{_libdir}/nagios/plugins/eventhandlers/redundancy-scenario1/
install -p -m 644 contrib/eventhandlers/redundancy-scenario1/handle-master-proc-event %{buildroot}%{_libdir}/nagios/plugins/eventhandlers/redundancy-scenario1/

%{__mv} contrib/README contrib/README.contrib

%pre common
getent group nagios >/dev/null || groupadd -r nagios
getent passwd nagios >/dev/null || useradd -r -g nagios -d %{_localstatedir}/spool/%{name} -s /sbin/nologin nagios
exit 0

%post
%{_sbindir}/usermod -a -G %{name} apache || :

%systemd_post %{name}.service  > /dev/null 2>&1 || :
/usr/bin/systemctl condrestart httpd > /dev/null 2>&1 || :
if [ $1 -gt 1 ]; then
  /usr/bin/systemctl reload nagios  > /dev/null 2>&1 || :
fi

%preun
%systemd_preun %{name}.service

%postun
/usr/bin/systemctl condrestart httpd  > /dev/null 2>&1 || :

%triggerun -- %{name} < 3.5.1-2
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply opensips
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save %{name} >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del %{name} >/dev/null 2>&1 || :
/bin/systemctl try-restart %{name}.service >/dev/null 2>&1 || :

%post selinux
%selinux_modules_install -s %{name} %{_datadir}/selinux/packages/%{selinuxtype}/%{name}.pp.bz2
%selinux_relabel_post -s %{name}

if [ "$1" -le "1" ]; then # First install
   %systemd_postun_with_restart %{name}.service
fi

%postun selinux
if [ $1 -eq 0 ]; then
    %selinux_modules_uninstall -s %{name} %{name}
    %selinux_relabel_post -s %{name}
    %systemd_postun_with_restart %{name}.service
fi

%files
%dir %{_libdir}/%{name}/cgi-bin
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/html
%doc %{_datadir}/%{name}/html/docs
%doc Changelog INSTALLING LICENSE README.md UPGRADING UpgradeToVersion4.ReadMe UpgradeToVersion4.sh
%doc internet.cfg
%{_datadir}/%{name}/html/[^cd]*
%{_datadir}/%{name}/html/contexthelp/
%{_sbindir}/*
%{_bindir}/*
%{_libdir}/%{name}/cgi-bin/*cgi
%{_unitdir}/%{name}.service
%{_tmpfilesdir}/%{name}.conf
%config(noreplace) %{_sysconfdir}/httpd/conf.d/nagios.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}
%config(noreplace) %{_sysconfdir}/%{name}/*cfg
%config(noreplace) %{_sysconfdir}/%{name}/objects/*cfg
%attr(0750,root,nagios) %dir %{_sysconfdir}/%{name}/private
%attr(0750,root,nagios) %dir %{_sysconfdir}/%{name}/objects

%attr(0640,root,nagios) %config(noreplace) %{_sysconfdir}/%{name}/private/resource.cfg
%attr(0640,root,apache) %config(noreplace) %{_sysconfdir}/%{name}/passwd
%attr(0640,root,apache) %config(noreplace) %{_datadir}/%{name}/html/config.inc.php
%attr(2775,nagios,nagios) %dir %{_localstatedir}/spool/%{name}/cmd
%attr(0750,nagios,nagios) %dir %{_localstatedir}/run/%{name}
%attr(0750,nagios,nagios) %dir %{_localstatedir}/log/%{name}
%attr(0750,nagios,nagios) %dir %{_localstatedir}/log/%{name}/archives
%attr(0770,nagios,nagios) %dir %{_localstatedir}/spool/%{name}/checkresults

%files common
%dir %{_sysconfdir}/%{name}
%dir %{_libdir}/%{name}
%attr(0755,root,root) %dir %{_libdir}/%{name}/plugins
%attr(0755,root,root) %dir %{_libdir}/%{name}/plugins/eventhandlers/
%attr(0755,nagios,nagios) %dir %{_localstatedir}/spool/%{name}

%files devel
%{_includedir}/%{name}
%attr(0644,root,root) %{_libdir}/%{name}/libnagios.a

%files selinux
%doc README.SELinux.rst
%{_datadir}/selinux/packages/%{name}/%{name}.pp.*
%{_datadir}/selinux/devel/include/distributed/%{name}.if
%ghost %verify(not md5 size mode mtime) %{_sharedstatedir}/selinux/%{name}/active/modules/200/%{name}


%files contrib
%doc contrib/README.contrib
%attr(0750,root,root) %{_libdir}/%{name}/plugins/eventhandlers/*
%{_libdir}/%{name}/cgi/

%changelog
* Thu Jan 16 2025 Funda Wang <fundawang@yeah.net> - 4.5.3-2
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Jun 20 2024 xu_ping <707078654@qq.com> - 4.5.3-1
- Update to 4.5.3

* Sat Oct 7 2023 liyanan <thistleslyn@163.com> - 4.4.14-1
- Update to 4.4.14

* Mon Aug 28 2023 chenchen <chen_aka_jan@163.com> - 4.4.13-2
- fix %postun error

* Thu Jul 27 2023 chenchen <chen_aka_jan@163.com> - 4.4.13-1
- Upgrade to version 4.4.13

* Tue Jan 18 2022 SimpleUpdate Robot <tc@openeuler.org> - 4.4.6-1
- Upgrade to version 4.4.6

* Mon Aug 2 2021 shdluan <shdluan@163.com> - 4.4.3-8
-fix pack failure when use gcc 10

* Thu Nov 26 2020 lingsheng <lingsheng@huawei.com> - 4.4.3-7
- Drop unexpected output in scripts

* Wed Apr 1 2020 yanglijin <yanglijin@huawei.com> - 4.4.3-6
- fix build failed

* Mon Dec 9 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.4.3-5
- Package init
